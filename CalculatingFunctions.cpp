#include <iostream>
#include <cstdlib>
#include <time.h>
#include "PrintingFunctions.h"

using namespace std;

void GenerateMatrix(int Matrix[17][17]) //���������� �������
{
	int i, j;
	srand(time(NULL));
	for (i = 0; i < 17; ++i)
	{
		for (j = 0; j < 17; ++j)
		{
			Matrix[i][j] = rand() % 21 - 10;
		}
	}
}

void SearchMaxElements(int Matrix[17][17]) //����� ������������ ��������� ������ �����
{
	setlocale(0, "");
	int i, j;
	int k = 0;
	int MaxElement = Matrix[1][1];
	int MEA[17 / 2]; //MEA - MaxElementsArray
	for (j = 2; j < 17; j += 2)
	{
		for (i = 1; i <= 17; ++i)
		{
			if (MaxElement < Matrix[i][j])
			{
				MaxElement = Matrix[i][j];
			}
		}
		MEA[k] = MaxElement;
		k += 1;
		MaxElement = 0;
	}
	PrintMaxElements(MEA);
}

void CoincidenceMainDiagonals(int Matrix[17][17]) //���������� ����������
{
	setlocale(0, "");
	int i, j;
	bool Coincidence = 1;
	for (i = 0; i < 17; ++i)
	{
		for (j = 0; j < 17; ++j)
		{
			if (Matrix[i][j] == Matrix[17 - i - 1][17 - j - 1])
			{
				Coincidence = 1;
			}
			else
			{
				Coincidence = 0;
				break;
			}
		}
		if (Coincidence == 0)
		{
			break;
		}
	}
	cout << "������� � �������� ��������� ";
	if (Coincidence == 1)
	{
		cout << "���������" << endl;
	}
	else
	{
		cout << "�� ���������" << endl;
	}
}

void SearchMinElement(int Matrix[17][17]) //����������� �������� �� �������
{
	setlocale(0, "");
	int i, j;
	int MinElement = Matrix[0][0];
	for (i = 0; i < 17; ++i)
	{
		for (j = 0; j < 17; ++j)
		{
			if ((i - j) >= 0)
			{
				if (MinElement > Matrix[i][j])
				{
					MinElement = Matrix[i][j];
				}
			}
		}
	}
	PrintMinElement(MinElement);
}