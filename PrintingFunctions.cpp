#include <iostream>
#include "PrintingFunctions.h""
#include <iomanip>

using namespace std;

void PrintMatrix(int Matrix[17][17])    // ����� �������
{
	int i, j;
	for (i = 0; i < 17; ++i)
	{
		for (j = 0; j < 17; ++j)
		{
			cout << Matrix[i][j]<<" ";
		}
		cout << endl;
	}
}

void PrintMaxElements(int MaxElements[8]) //����� ������������ ��������� ������ �����
{
	int i;
	
	for (i = 0; i < 8; ++i)
	{
		cout << "������� " << (i + 1) * 2 << ": " << MaxElements[i] << endl;
	}
}

void PrintMinElement(int MinElement) //����� ������������ �������� �������
{
	cout << "����������� ������� �������: " << MinElement << endl;
}